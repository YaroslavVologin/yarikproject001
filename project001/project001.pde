/*
Суть программы. 
 Поле состоит из 3 границ - все, кроме верхней.                       +
 Сверху падают фигуры (рандомно - шарик/квадрат).                     -
 */
import fisica.*;
FWorld world;
int number;
FBox leftBorder;
FBox rightBorder;
FBox downBorder;

void setup() {
  Fisica.init(this);

  size(400, 400);
  background(100);

  world = new FWorld();

  leftBorder = new FBox(10, 400);
  leftBorder.setPosition(5, 200);  
  leftBorder.setStatic(true);
  rightBorder = new FBox(10, 400);
  rightBorder.setPosition(395, 200);
  rightBorder.setStatic(true);  
  downBorder = new FBox(400, 10);
  downBorder.setPosition(200, 395);
  downBorder.setStatic(true);

  world.add(leftBorder);
  world.add(rightBorder);
  world.add(downBorder);
}

void draw() {
  background(100);
  world.draw();
  world.step();

  if (number < millis()/500) {
    NewBody();
    number++;
  }
}

void NewBody() {
  int i = (int)random(2);
  if (i == 0) {
    FBox newBody = new FBox(20 + random(20), 20 + random(20));
    newBody.setPosition(40 + random(width - 80), 0);
    world.add(newBody);
  } else {
    FCircle newBody = new FCircle(20 + random(20));
    newBody.setPosition(40 + random(width - 80), 0);
    world.add(newBody);
  }
}

void contactStarted(FContact cont) {
  FBody b1 = cont.getBody1();
  FBody b2 = cont.getBody2();
  if (b1 != leftBorder && b2 != leftBorder &&
    b1 != rightBorder && b2 != rightBorder &&
    b1 != downBorder && b2 != downBorder) {

    world.remove(b1);
    world.remove(b2);
  }
}